package com.example.miguel.mobilecouchbase;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.MutableDocument;
import com.couchbase.lite.ResultSet;
import com.couchbase.lite.internal.support.Log;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.w3c.dom.Text;

import java.util.UUID;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";
    public static final String INTENT_LIST_ID = "item_id";

    private Application application;
    private Database mDatabase = null;
    private ResultSet result = null;
    // This is the Adapter being used to display the list's data
    private ListsAdapter mAdapter;
    private ListView listaRecycle;
    private EditText descriptionInput;
    private EditText codigoInput;
    private AutoCompleteTextView companyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayCreateListDialog();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Definir nombres para Navigation Bar
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        View headerLayout = navigationView.getHeaderView(0);
        TextView nameText = (TextView) headerLayout.findViewById(R.id.nameBar);
        nameText.setText(account.getDisplayName());
        TextView emailText = (TextView) headerLayout.findViewById(R.id.textView);
        emailText.setText(account.getEmail());

        showListView("todos");

    }

    public void showListView(String opcion){
        //Proceso de carga de lista
        application = (Application) getApplication();
        mDatabase = application.getDatabase();

        listaRecycle = (ListView) findViewById(R.id.listRecycle);
        // Create an empty adapter we will use to display the loaded data.
        // We pass null for the cursor, then update it in onLoadFinished()
        mAdapter = new ListsAdapter(this, mDatabase, opcion);
        listaRecycle.setAdapter(mAdapter);
        listaRecycle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String id = mAdapter.getItem(i);
                Document list = mDatabase.getDocument(id);
                showTaskListView(list);
            }
        });
        listaRecycle.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int pos, long id) {
                PopupMenu popup = new PopupMenu(MainActivity.this, view);
                popup.inflate(R.menu.list_item);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String id = mAdapter.getItem(pos);
                        Document list = mDatabase.getDocument(id);
                        return handleListPopupAction(item, list);
                    }
                });
                popup.show();
                return true;
            }
        });
    }

    private boolean handleListPopupAction(MenuItem item, Document list) {
        switch (item.getItemId()) {
            case R.id.update:
                if(list.getString("estado").equals("disponible")) {
                    displayUpdateListDialog(list);
                }else{
                    AlertDialog.Builder mensaje = new AlertDialog.Builder(this);
                    mensaje.setTitle("No se puede elimar este registro");
                    mensaje.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    });
                    mensaje.show();
                }
                return true;
            case R.id.delete:
                if(list.getString("estado").equals("pendiente")){
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle("Eliminar registro");
                    TextView textView = new TextView(this);
                    textView.setText("Realmente desea elminar el registro?");
                    alert.setView(textView);
                    final Document lista = list;
                    alert.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            deleteList(lista);
                        }
                    });
                    alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    });
                    alert.show();
                }else{
                    AlertDialog.Builder mensaje = new AlertDialog.Builder(this);
                    mensaje.setTitle("No se puede elimar este registro");
                    mensaje.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    });
                    mensaje.show();
                }
                return true;
            default:
                return false;
        }
    }

    private void showTaskListView(Document list){
        Intent intent = new Intent(this, ItemDetailActivity.class);
        intent.putExtra(INTENT_LIST_ID, list.getId());
        startActivity(intent);
    }

    // display create list dialog
    private void displayCreateListDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getResources().getString(R.string.title_dialog_new_list));
        final View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.view_dialog_input, null);
        descriptionInput = (EditText) view.findViewById(R.id.descripcion);
        codigoInput = (EditText) view.findViewById(R.id.codigo);
        // Get a reference to the AutoCompleteTextView in the layout
        companyView = (AutoCompleteTextView) view.findViewById(R.id.autoCompany);
        // Get the string array
        String[] countries = getResources().getStringArray(R.array.companys_array);
        // Create the adapter and set it to the AutoCompleteTextView
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, countries);
        companyView.setAdapter(adapter);
        alert.setView(view);
        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String descripcion = descriptionInput.getText().toString();
                String company = companyView.getText().toString();
                String codigo = codigoInput.getText().toString();
                if (descripcion.length() == 0 || company.length() == 0 || codigo.length() == 0)
                    return;
                createList(descripcion, company, codigo);

            }
        });
        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alert.show();
    }

    // display update list dialog
    private void displayUpdateListDialog(final Document list) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getResources().getString(R.string.title_dialog_update));
        final EditText input = new EditText(this);
        input.setMaxLines(1);
        input.setSingleLine(true);
        input.setHint("Ingrese código de barra");
        alert.setView(input);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String title = input.getText().toString();
                if (title.length() == 0)
                    return;
                updateList(list.toMutable(), title);
            }
        });
        alert.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.salir) {
            signOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            showListView("todos");
        } else if (id == R.id.nav_gallery) {
            showListView("mios");
        } else if (id == R.id.nav_slideshow) {
            showListView("pendientes");
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void signOut() {
        Application application = (Application) getApplication();
        application.signOut();
    }

    // -------------------------
    // Database - CRUD
    // -------------------------

    // create list
    private Document createList(String title, String company, String codigo) {
        //Obtener cuenta de google
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

        String docId = UUID.randomUUID().toString();
        MutableDocument mDoc = new MutableDocument(docId);
        mDoc.setString("company", company);
        mDoc.setString("descripcion", title);
        mDoc.setString("codigo", codigo);
        mDoc.setString("estado", "pendiente");
        mDoc.setString("usuario", account.getEmail());

        try {
            mDatabase.save(mDoc);
            return mDatabase.getDocument(mDoc.getId());
        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Failed to save the doc - %s", e, mDoc);
            //TODO: Error handling
            return null;
        }
    }

    // update list
    private Document updateList(final MutableDocument list, String title) {
        //Obtener cuenta de google
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

        list.setString("codigo", title);
        list.setString("estado", "pendiente");
        list.setString("usuario", account.getEmail());

        try {
            mDatabase.save(list);
            return mDatabase.getDocument(list.getId());
        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Failed to save the doc - %s", e, list);
            //TODO: Error handling
            return null;
        }
    }

    // delete list
    private Document deleteList(final Document list) {
        try {
            mDatabase.delete(list);
        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Failed to delete the doc - %s", e, list);
            //TODO: Error handling
        }
        return list;
    }
}
