package com.example.miguel.mobilecouchbase;

import android.content.Intent;
import android.util.Log;

import com.couchbase.lite.BasicAuthenticator;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.DatabaseConfiguration;
import com.couchbase.lite.Endpoint;
import com.couchbase.lite.Replicator;
import com.couchbase.lite.ReplicatorChange;
import com.couchbase.lite.ReplicatorChangeListener;
import com.couchbase.lite.ReplicatorConfiguration;
import com.couchbase.lite.URLEndpoint;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;

import java.net.URI;
import java.net.URISyntaxException;

public class Application extends android.app.Application {

    private static final String TAG = "Application";
    private GoogleSignInClient mGoogleSignInClient;

    private Database mDatabase = null;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public Database getDatabase() {
        return mDatabase;
    }

    private void setDatabase(Database database) {
        this.mDatabase = database;
    }

    private Database getDb() {
        // Get the database (and create it if it doesn’t exist).
        DatabaseConfiguration config = new DatabaseConfiguration(getApplicationContext());
        try {
            return new Database("plastico", config);
        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Cannot create database. " + e.getMessage());
        }
        return null;
    }

    public void initDatabase() {
        setDatabase(getDb());
        startReplicator();
    }

    public ReplicatorConfiguration configReplicator(Database database) {
        //Create replicators to push and pull changes to and from the cloud.
        Endpoint targetEndpoint = null;
        try {
            targetEndpoint = new URLEndpoint(new URI("ws://201.227.172.43:4984/plastico"));
        } catch (URISyntaxException e) {
            Log.e(TAG, "Error trying to communicate with server :: " + e.getMessage());
        }
        ReplicatorConfiguration replConfig = new ReplicatorConfiguration(database, targetEndpoint);
        replConfig.setReplicatorType(ReplicatorConfiguration.ReplicatorType.PUSH_AND_PULL);
        replConfig.setContinuous(true);

        // Add authentication.
        replConfig.setAuthenticator(new BasicAuthenticator("mobile", "Contra123"));

        return replConfig;
    }

    public void startReplicator() {
        // Create replicator.
        Replicator replicator = new Replicator(configReplicator(mDatabase));

        // Listen to replicator change events.
        replicator.addChangeListener(new ReplicatorChangeListener() {
            @Override
            public void changed(ReplicatorChange change) {
                if (change.getStatus().getError() != null) {
                    com.couchbase.lite.internal.support.Log.i(TAG, "Error code ::  " + change.getStatus().getError().getCode());
                    Integer error = change.getStatus().getError().getCode();
                    Log.e(TAG, "Error in Replicator Status :: " + error);
                }
            }
        });

        // Start replication.
        replicator.start();
    }

    //LogOut Application
    public void signOut() {
        Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
        intent.setAction(SignInActivity.ACTION_LOGOUT);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
