package com.example.miguel.mobilecouchbase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.couchbase.lite.DataSource;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Expression;
import com.couchbase.lite.Function;
import com.couchbase.lite.Meta;
import com.couchbase.lite.Ordering;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryBuilder;
import com.couchbase.lite.QueryChange;
import com.couchbase.lite.QueryChangeListener;
import com.couchbase.lite.Result;
import com.couchbase.lite.ResultSet;
import com.couchbase.lite.SelectResult;
import com.couchbase.lite.internal.support.Log;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.util.HashMap;
import java.util.Map;

public class ListsAdapter extends ArrayAdapter<String> {
    private static final String TAG = ListsAdapter.class.getSimpleName();

    private Database db;
    private Query listsQuery = null;
    private Query incompTasksCountQuery = null;
    private Map<String, Integer> incompCounts = new HashMap<>();

    public ListsAdapter(Context context, Database db, String opcion) {
        super(context, 0);

        if (db == null) throw new IllegalArgumentException();
        this.db = db;

        //Evaluar opcion para busqueda de lista
        switch (opcion){
            case "todos":
                this.listsQuery = listsQuery();
                break;
            case "mios":
                this.listsQuery = listMiosQuery();
                break;
            case "pendientes":
                this.listsQuery = listPendientesQuery();
                break;
        }
        //Determinar eventos
        this.listsQuery.addChangeListener(new QueryChangeListener() {
            @Override
            public void changed(QueryChange change) {
                clear();
                ResultSet rs = change.getResults();
                Result result;
                while ((result = rs.next()) != null) {
                    add(result.getString(0));
                }
                notifyDataSetChanged();
            }
        });

        this.incompTasksCountQuery = incompTasksCountQuery();
        this.incompTasksCountQuery.addChangeListener(new QueryChangeListener() {
            @Override
            public void changed(QueryChange change) {
                incompCounts.clear();
                ResultSet rs = change.getResults();
                Result result;
                while ((result = rs.next()) != null) {
                    Log.e(TAG, "result -> " + result.toMap());
                    incompCounts.put(result.getString(0), result.getInt(1));
                }
                notifyDataSetChanged();
            }
        });
    }

    private Query listMiosQuery() {
        //Obtener cuenta de google
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getContext());

        return QueryBuilder.select(SelectResult.expression(Meta.id))
                .from(DataSource.database(db))
                .where(Expression.property("usuario").equalTo(Expression.string(account.getEmail()))
                        .and(Expression.property("estado").equalTo(Expression.string("reciclado"))));
    }

    private Query listPendientesQuery() {
        //Obtener cuenta de google
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getContext());

        return QueryBuilder.select(SelectResult.expression(Meta.id))
                .from(DataSource.database(db))
                .where(Expression.property("usuario").equalTo(Expression.string(account.getEmail()))
                        .and(Expression.property("estado").equalTo(Expression.string("pendiente"))));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String id = getItem(position);
        Document list = db.getDocument(id);
        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.view_list, parent, false);

        TextView text = convertView.findViewById(R.id.text);
        text.setText(list.getString("descripcion"));

        TextView countText = convertView.findViewById(R.id.task_count);
        if (incompCounts.get(list.getId()) != null) {
            countText.setText(String.valueOf(((int) incompCounts.get(list.getId()))));
        } else {
            countText.setText("");
        }

        Log.e(TAG, "getView(): pos -> %d, docID -> %s, name -> %s, name2 -> %s, all -> %s", position, list.getId(), list.getString("company"), list.getValue("company"), list.toMap());
        return convertView;
    }

    private Query listsQuery() {
        return QueryBuilder.select(SelectResult.expression(Meta.id))
                .from(DataSource.database(db))
                .where(Expression.property("estado").equalTo(Expression.string("disponible")));
                //.orderBy(Ordering.property("name").ascending());
    }

    private Query incompTasksCountQuery() {
        Expression exprType = Expression.property("company");
        Expression exprComplete = Expression.property("estado");
        Expression exprTaskListId = Expression.property("objeto.id");
        SelectResult srTaskListID = SelectResult.expression(exprTaskListId);
        SelectResult srCount = SelectResult.expression(Function.count(Expression.all()));
        return QueryBuilder.select(srTaskListID, srCount)
                .from(DataSource.database(db))
                .where(exprType.equalTo(Expression.string("coca-cola")).and(exprComplete.equalTo(Expression.booleanValue(false))))
                .groupBy(exprTaskListId);
    }
}
