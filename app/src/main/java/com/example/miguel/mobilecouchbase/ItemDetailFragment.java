package com.example.miguel.mobilecouchbase;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.couchbase.lite.Database;
import com.couchbase.lite.Document;

import org.w3c.dom.Text;

public class ItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    private Database db;
    private Document mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            db = ((Application) getActivity().getApplication()).getDatabase();
            mItem = db.getDocument(getArguments().getString(ARG_ITEM_ID)) ;

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle("Detalles reciclaje");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);

        // Show the dummy content as text in a TextView.
        TextView textViewEstado = (TextView) rootView.findViewById(R.id.estado);
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.item_detail)).setText(mItem.getString("descripcion"));
            textViewEstado.setText(mItem.getString("estado"));
            ((TextView) rootView.findViewById(R.id.company)).setText("Compañia : " + mItem.getString("company"));
            if(!mItem.getString("estado").equals("disponible")) {
                textViewEstado.setBackgroundColor(Color.RED);
                ((TextView) rootView.findViewById(R.id.codigo)).setText("Código : " + mItem.getString("codigo"));
            }else{
                textViewEstado.setBackgroundColor(Color.GREEN);
            }
        }

        return rootView;
    }
}